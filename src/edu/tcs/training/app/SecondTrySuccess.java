package edu.tcs.training.app;


import java.util.HashMap;
import java.util.Map;


public class SecondTrySuccess {

	//The problem requires to return a String as answer
	public static String getTotalBill(Map<String, Integer> itemDetails) {

		double totalItems = 0;
		
		for (int i = 0; i < itemDetails.size(); i++) {
			
		}
		for (Map.Entry<String, Integer> entry : itemDetails.entrySet()) {
	
	
			int value = entry.getValue();
	
			
			//The problem was that I was trying to use == instead of equals
			if (entry.getKey().equals("apple")) {
				totalItems += value * 2.0;
			} else if (entry.getKey().equals("orange")) { 
				totalItems += value * 1.5;
			} else if (entry.getKey().equals("mango")){
				totalItems += value * 1.2;
			} else if (entry.getKey().equals("grape")) {
				totalItems += value * 1.0;
			}


		}

		// Write your code here
		String totalString = Double.toString(totalItems);
		
		return totalString;

	}

	public static void main(String[] args) {

		//String in the format required by the problem
		String items = "apple 30 orange 10 mango 20 grape 5";
		
		
		String[] separatedItems = items.split(" ");
		
		Map<String, Integer> map = new HashMap<String,Integer>();
		
		//Adding elements to the map object
		for (int i = 0; i < separatedItems.length; i+=2) {
		
			map.put(separatedItems[i], Integer.parseInt(separatedItems[i+1]));
	
		}

		String totalBill = getTotalBill(map); 
		System.out.println(totalBill);

	}
}
