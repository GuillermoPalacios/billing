package edu.tcs.training.app;


import java.util.HashMap;
import java.util.Map;


public class FirstTryWrong {

	//The problem requires to return a String as answer
	public static String getTotalBill(Map<String, Integer> itemDetails) {

		double totalItems = 0;
	
		        for (Map.Entry<String, Integer> entry : itemDetails.entrySet()) {
		            String key = entry.getKey();
//		            System.out.println(key);    Testing purposes
		            int value = entry.getValue();
//		            System.out.println(value); Testing purposes
		            if (key == "Apple") {
		                totalItems += value * 2.0;

		            } else if (key == "Orange") {
		                totalItems += value * 1.5;
		            } else if (key == "Mango") {
		                totalItems += value * 1.2;
		            } else if (key == "Grape") {
		                totalItems += value * 1.0;
		            }


		        }
		        System.out.println("This is "+totalItems);
		        // Write your code here
		        String totalString = Double.toString(totalItems);
		        System.out.println("Hola mund");
		        System.out.println(totalString);
		        return totalString;

	}

	public static void main(String[] args) {

		//String in the format required by the problem
		String items = "apple 30 orange 10 mango 20 grape 5";
		
		
		String[] separatedItems = items.split(" ");
		
		Map<String, Integer> map = new HashMap<String,Integer>();

//		Adding elements with put	
//		map.put("Orange", 50); 
//		map.put("Apple", 50); 
// 		If I add the elements in this way my program runs without problems		
		
		//If I tried to add new elements using this approach it will failed
		//Adding elements to the map object
		for (int i = 0; i < separatedItems.length; i+=2) {
		
			map.put(separatedItems[i], Integer.parseInt(separatedItems[i+1]));
	
		}

		String totalBill = getTotalBill(map); 
		System.out.println(totalBill);

	}
}
